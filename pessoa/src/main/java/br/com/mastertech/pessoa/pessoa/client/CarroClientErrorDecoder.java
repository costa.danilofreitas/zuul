package br.com.mastertech.pessoa.pessoa.client;

import br.com.mastertech.pessoa.pessoa.exception.CarroNotFoundException;
import feign.Response;
import feign.RetryableException;
import feign.codec.ErrorDecoder;

public class CarroClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new CarroNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
