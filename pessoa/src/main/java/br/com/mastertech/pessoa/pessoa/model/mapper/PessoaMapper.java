package br.com.mastertech.pessoa.pessoa.model.mapper;

import br.com.mastertech.pessoa.pessoa.client.CarroClient;
import br.com.mastertech.pessoa.pessoa.client.CarroDTO;
import br.com.mastertech.pessoa.pessoa.model.Pessoa;
import br.com.mastertech.pessoa.pessoa.model.dto.GetPessoaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PessoaMapper {

    @Autowired
    private CarroClient carroClient;

    public GetPessoaResponse toGetResponse(Pessoa pessoa) {
        CarroDTO carroDTO = carroClient.getById(pessoa.getCarroId());

        GetPessoaResponse getPessoaResponse = new GetPessoaResponse();
        getPessoaResponse.setId(pessoa.getId());
        getPessoaResponse.setName(pessoa.getName());
        getPessoaResponse.setCarroDTO(carroDTO);

        return getPessoaResponse;
    }
}
