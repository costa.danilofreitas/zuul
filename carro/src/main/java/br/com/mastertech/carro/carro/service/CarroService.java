package br.com.mastertech.carro.carro.service;

import br.com.mastertech.carro.carro.exception.CarroNotFoundException;
import br.com.mastertech.carro.carro.model.Carro;
import br.com.mastertech.carro.carro.repository.CarroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarroService {

    @Autowired
    private CarroRepository carroRepository;

    public Carro create(Carro carro) {
        return carroRepository.save(carro);
    }

    public Carro getById(Long id) {
        Optional<Carro> byId = carroRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CarroNotFoundException();
        }

        return byId.get();
    }

}
