package br.com.mastertech.carro.carro.controller;

import br.com.mastertech.carro.carro.model.Carro;
import br.com.mastertech.carro.carro.service.CarroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/carro")
public class CarroController {

    @Autowired
    private CarroService carroService;

    @PostMapping
    public Carro create(@RequestBody Carro carro) {
        return carroService.create(carro);
    }

    @GetMapping("/{id}")
    public Carro getById(@PathVariable Long id) {
        System.out.println("Buscaram um carro de id " + id + " em " + System.currentTimeMillis());
        return carroService.getById(id);
    }
}
